package dsa.onlineserver;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import dsa.control.Version;
import dsa.remote.IRemoteLog;
import dsa.remote.ServerManager;

public class OnlineServer implements IRemoteLog {
	
	private static void printUsage() {
		System.out.println("Verwendung: hvonlineserver ipaddress port [loglevels]");
		System.out.println("   ipaddress: entweder Adresse oder Hostname");
		System.out.println("   port: zwischen 1000 und 2000");
		System.out.println("   loglevels: oder-Verknüpfung von 1 (Fehler), 2 (Verwaltung), 4 (Spiel)");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Heldenverwaltung Online-Server Version " + Version.getCurrentVersionString());
		if (args.length < 2 || args.length > 3) {
			printUsage();
			return;
		}
		int port = 0;
		try {
			port = Integer.parseInt(args[1]);
		}
		catch (NumberFormatException e) {
		}
		if (port < 1000 || port > 2000) {
			printUsage();
			return;
		}
		int logLevel = -1;
		if (args.length == 3) {
			try {
				logLevel = Integer.parseInt(args[2]);
			}
			catch (NumberFormatException e) {
			}
		}
		else {
			logLevel = 7;
		}
		if (logLevel < 0 || logLevel > 7) {
			printUsage();
			return;
		}
		int result = 0;
		try {
			OnlineServer server = new OnlineServer(args[0], port, logLevel);
			System.out.println("Starte Server auf " + args[0] + ":" + port);
			server.start();
			System.out.println("Return zum Beenden ...");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			br.readLine();
			server.stop();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			result = 1;
		}
		System.out.println("Server beendet.");
		System.exit(result);
	}
	
	private String address;
	private int port;
	private int logLevel;
	
	private OnlineServer(String ipaddress, int port, int logLevel) {
		address = ipaddress;
		this.port = port;
		this.logLevel = logLevel;
		ServerManager.getInstance().setRemoteLogClient(this);
	}
	
	private void start() {
		System.setProperty("java.rmi.server.hostname", address); //$NON-NLS-1$
		ServerManager.getInstance().startServer(port);
	}
	
	private void stop() {
		ServerManager.getInstance().stopServer();
	}

	@Override
	public void addLog(LogCategory category, String message) {
		switch (category) {
		case Game:
			if ((logLevel & 4) != 0) {
				System.out.println("Spiel: " + message);
			}
			break;
		case Management:
			if ((logLevel & 2) != 0) {
				System.out.println("Verwaltung: " + message);
			}
			break;
		case Error:
			if ((logLevel & 1) != 0) {
				System.out.println("Fehler: " + message);
			}
			break;
		default:
			System.out.println("Unbekannt: " + message);
			break;
		}
	}

	@Override
	public void connectionStatusChanged() {
		System.out.println("Fehler: Verbindung verloren.");
	}

}
