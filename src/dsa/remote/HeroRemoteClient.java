/*
    Copyright (c) 2006-2015 [Joerg Ruedenauer]
  
    This file is part of Heldenverwaltung.

    Heldenverwaltung is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Heldenverwaltung is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Heldenverwaltung; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.remote;

import java.rmi.RemoteException;
import java.util.HashSet;

import dsa.model.characters.CharacterObserver;
import dsa.model.characters.Energy;
import dsa.model.characters.Group;
import dsa.model.characters.GroupObserver;
import dsa.model.characters.Hero;
import dsa.model.characters.Property;
import dsa.model.characters.Hero.DerivedValue;

abstract class HeroRemoteClient extends RemoteClient implements CharacterObserver, GroupObserver {

	protected HashSet<Hero> mHeroesOnline = new HashSet<Hero>();
	
	protected abstract boolean needsHeroObserver();
	
	public void connectHeroOnline(Hero hero) {
		if (mHeroesOnline.contains(hero))
			return;
		if (!isConnected())
			return;
		try {
			getServer().addHero(getClientId(), hero.getName());
			String serializedHero = hero.storeToString();
			if (serializedHero != null && !serializedHero.isEmpty()) {
				getServer().updateHero(getClientId(), hero.getName(), serializedHero);
			}
			else {
				for (Energy energy : Energy.values()) {
					getServer().informGMOfEnergyChange(getClientId(), hero.getName(), energy, hero.getCurrentEnergy(energy));
				}
				for (Property property : Property.values()) {
					getServer().informGMOfPropertyChange(getClientId(), hero.getName(), property, hero.getCurrentProperty(property));
				}
			}
			if (getLog() != null) {
				getLog().addLog(IRemoteLog.LogCategory.Management, hero.getName() + " am Server angemeldet.");
			}
			mHeroesOnline.add(hero);
			if (needsHeroObserver())
				hero.addObserver(this);
		}
		catch (RemoteException re) {
			handleRemoteException(re);
		}
		catch (ServerException se) {
			handleServerException(se);
		}
	}
	
	private void updateHeroOnline(Hero hero) {
		if (!mHeroesOnline.contains(hero))
			return;
		if (!isConnected())
			return;
		try {
			String serializedHero = hero.storeToString();
			if (serializedHero != null && !serializedHero.isEmpty()) {
				getServer().updateHero(getClientId(), hero.getName(), serializedHero);
			}
			if (getLog() != null) {
				getLog().addLog(IRemoteLog.LogCategory.Management, hero.getName() + " am Server aktualisiert.");
			}
		}
		catch (RemoteException re) {
			handleRemoteException(re);
		}
		catch (ServerException se) {
			handleServerException(se);
		}		
	}
	
	public boolean isHeroConnectedOnline(Hero hero) {
		if (hero == null)
			return false;
		return mHeroesOnline.contains(hero);
	}
	
	public void disconnectHeroOnline(Hero hero) {
		if (!mHeroesOnline.contains(hero))
			return;
		if (!isConnected())
			return;
		try {
			getServer().removeHero(getClientId(), hero.getName());
			if (getLog() != null) {
				getLog().addLog(IRemoteLog.LogCategory.Management, hero.getName() + " vom Server abgemeldet.");
			}
			mHeroesOnline.remove(hero);
			if (needsHeroObserver())
				hero.removeObserver(this);
		}
		catch (RemoteException re) {
			handleRemoteException(re);
		}
		catch (ServerException se) {
			handleServerException(se);
		}
	}
	
	@Override
	protected void onConnect() {
		mActiveHero = Group.getInstance().getActiveHero();
		Group.getInstance().addObserver(this);
	}
	
	@Override
	protected void onDisconnect() {
		if (needsHeroObserver()) {
			for (Hero hero : mHeroesOnline) {
				hero.removeObserver(this);
			}
		}
		Group.getInstance().removeObserver(this);
		mHeroesOnline.clear();
	}
	
	@Override
	protected void onConnectionLost() {
		if (needsHeroObserver()) {
			for (Hero hero : mHeroesOnline) {
				hero.removeObserver(this);
			}
		}
		Group.getInstance().removeObserver(this);
		mHeroesOnline.clear();		
	}
	
	protected Hero mActiveHero = Group.getInstance().getActiveHero();

	@Override
	public void activeCharacterChanged(Hero newCharacter, Hero oldCharacter) {
		mActiveHero = newCharacter;
	}

	@Override
	public void characterRemoved(Hero character) {
		if (!mListenForChanges)
			return;
		if (mHeroesOnline.contains(character)) {
			disconnectHeroOnline(character);
		}
	}

	@Override
	public void characterAdded(Hero character) {
	}

	@Override
	public void globalLockChanged() {
	}

	@Override
	public void talentAdded(String talent) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void talentRemoved(String talent) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void defaultTalentChanged(String talent) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void currentTalentChanged(String talent) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void defaultPropertyChanged(Property property) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void currentPropertyChanged(Property property) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		try {
			getServer().informGMOfPropertyChange(getClientId(), mActiveHero.getName(), property, mActiveHero.getCurrentProperty(property));
		}
		catch (RemoteException re) {
			handleRemoteException(re);
		}
		catch (ServerException se) {
			handleServerException(se);
		}				
	}

	@Override
	public void defaultEnergyChanged(Energy energy) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void currentEnergyChanged(Energy energy) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		try {
			getServer().informGMOfEnergyChange(getClientId(), mActiveHero.getName(), energy, mActiveHero.getCurrentEnergy(energy));
		}
		catch (RemoteException re) {
			handleRemoteException(re);
		}
		catch (ServerException se) {
			handleServerException(se);
		}				
	}

	@Override
	public void stepIncreased() {
	}

	@Override
	public void increaseTriesChanged() {
	}

	@Override
	public void weightChanged() {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void atPADistributionChanged(String talent) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void derivedValueChanged(DerivedValue dv) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void thingRemoved(String thing, boolean fromWarehouse) {
		// information transferred through weight change
	}

	@Override
	public void weaponRemoved(String weapon) {
		// information transferred through weight change
	}

	@Override
	public void armourRemoved(String armour) {
		// information transferred through weight change
	}

	@Override
	public void shieldRemoved(String name) {
		// information transferred through weight change
	}

	@Override
	public void nameChanged(String oldName, String newName) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		try {
			getServer().changeHeroName(getClientId(), oldName, newName);
		}
		catch (RemoteException re) {
			handleRemoteException(re);
		}
		catch (ServerException se) {
			handleServerException(se);
		}				
	}

	@Override
	public void bfChanged(String item) {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void beModificationChanged() {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		updateHeroOnline(mActiveHero);
	}

	@Override
	public void thingsChanged() {
		// information transferred through weight change
	}

	@Override
	public void activeWeaponsChanged() {
		if (!mListenForChanges || !isConnected())
			return;
		if (!mHeroesOnline.contains(mActiveHero))
			return;
		try {
			getServer().informGMOfWeaponChange(getClientId(), mActiveHero.getName(), mActiveHero.getFightMode(), 
					mActiveHero.getFirstHandWeapon(), mActiveHero.getSecondHandItem(), true);
		}
		catch (RemoteException re) {
			handleRemoteException(re);
		}
		catch (ServerException se) {
			handleServerException(se);
		}						
	}

	@Override
	public void fightingStateChanged() {
		// information about dazed / grounded is explicitly called
	}

	@Override
	public void opponentWeaponChanged(int weaponNr) {
	}

	@Override
	public void moneyChanged() {
		// information transferred through weight change
	}

}
