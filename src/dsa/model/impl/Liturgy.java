package dsa.model.impl;

import dsa.model.talents.ILiturgy;

class Liturgy extends RitualBase implements ILiturgy {

	public Liturgy(String n, boolean kas, String r, TestData td,
			LearningTestData ltd, int d) {
		super(n, kas, r, td, ltd);
		degree = d;
	}

	@Override
	public int getDegree() {
		return degree;
	}

	@Override
	public RitualType getRitualType() {
		return RitualType.Liturgy;
	}
	
	private int degree;

}
