/*
    Copyright (c) 2006-2014 [Joerg Ruedenauer]
  
    This file is part of Heldenverwaltung.

    Heldenverwaltung is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Heldenverwaltung is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Heldenverwaltung; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.model.impl;

import dsa.model.talents.ICurse;

class Curse extends RitualBase implements ICurse {

	public Curse(String n, boolean kas, String r, TestData td,
			LearningTestData ltd, int asp, boolean perm) {
		super(n, kas, r, td, ltd);
		cost = asp;
		permanentAllowed = perm;
	}

	@Override
	public RitualType getRitualType() {
		return RitualType.Curse;
	}
	
	@Override
	public boolean permanentAllowed() {
		return permanentAllowed;
	}

	@Override
	public int getCost(Technique technique, boolean permanent) {
		if (permanent) {
			return technique == Technique.Rage ? 10 * cost : 5 * cost;
		}
		switch (technique) {
		case Rage:
		case Sleep:
		case Object:
			return 2 * cost;
		default:
			return cost;
		}
		
	}
	
	private int cost;
	private boolean permanentAllowed;

}
