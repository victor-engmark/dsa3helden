package dsa.model.impl;

class SimpleRitual extends RitualBase {

	public SimpleRitual(String n, boolean kas, String r, TestData td,
			LearningTestData ltd) {
		super(n, kas, r, td, ltd);
	}

	@Override
	public RitualType getRitualType() {
		return RitualType.Other;
	}

}
