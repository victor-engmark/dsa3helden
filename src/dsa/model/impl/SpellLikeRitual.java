/*
    Copyright (c) 2006-2014 [Joerg Ruedenauer]
  
    This file is part of Heldenverwaltung.

    Heldenverwaltung is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Heldenverwaltung is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Heldenverwaltung; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.model.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import dsa.model.talents.ISpellLikeAbility;
import dsa.model.talents.ISpellLikeRitual;

class SpellLikeRitual extends RitualBase implements ISpellLikeRitual {

	public SpellLikeRitual(String n, boolean kas, String r, TestData td,
			LearningTestData ltd, int line, String costs) throws java.io.IOException {
		super(n, kas, r, td, ltd);
		StringTokenizer tokenizer = new StringTokenizer(costs, ",");
		SpellImpl.parseCosts(tokenizer, mVariants, mCosts);
	}
	
	private ArrayList<String> mVariants = new ArrayList<String>();
	private ArrayList<ArrayList<ISpellLikeAbility.Cost>> mCosts = new ArrayList<ArrayList<ISpellLikeAbility.Cost>>(); 

	@Override
	public int getNrOfVariants() {
		return mVariants.size();
	}

	@Override
	public String getVariantName(int variant) {
		return mVariants.get(variant);
	}

	@Override
	public List<ISpellLikeAbility.Cost> getCosts(int variant) {
		return mCosts.get(variant);
	}

	@Override
	public RitualType getRitualType() {
		return RitualType.SpellLike;
	}

}
