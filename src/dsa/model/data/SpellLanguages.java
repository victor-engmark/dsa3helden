/*
 Copyright (c) 2006-2015 [Joerg Ruedenauer]
 
 This file is part of Heldenverwaltung.

 Heldenverwaltung is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Heldenverwaltung is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Heldenverwaltung; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.model.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import dsa.model.talents.Talent;

public class SpellLanguages {
	
	public enum Language
	{
		Keine, 
		Garethi,
		Tulamidya,
		Zhayad,
		Zyklopäisch,
		Bosparano,
		Isdira,
		Thorwalsch
	}

	private static SpellLanguages instance = new SpellLanguages();
	
	private static boolean dataRead = false;
	
	private static class SpellData	{

		private final ArrayList<String> translations;
		private final String spellName;
		
		public SpellData(String inputFileLine) throws IOException {
			String[] tokens = inputFileLine.split(";");
			int nrOfTokens = tokens.length;
			if (nrOfTokens < 1) {
				throw new IOException("Zaubername fehlt!");
			}
			if (nrOfTokens > Language.values().length - 1) { // no value for "keine", no value for "Garethi"
				throw new IOException("Mehr Sprachen als bekannt!");
			}
			spellName = tokens[0];
			Talent t = Talents.getInstance().getTalent(spellName);
			if (t == null) {
				throw new IOException("Unbekannter Zauber '" + spellName + "'!");
			}
			if (!t.isSpell()) {
				throw new IOException(spellName + " ist kein Zauber!");
			}
			translations = new ArrayList<String>();
			translations.add(spellName); // Garethi
			for (int i = 1; i < tokens.length; ++i) {
				translations.add(tokens[i]);
			}
		}
		
		public String getSpellName() { 
			return spellName;
		}
		
		public String getTranslation(Language l) {
			int index = l.ordinal() - 1; // no values for "keine"
			if (index >= translations.size()) {
				return "";
			}
			return translations.get(index);
		}
		
	}
	
	private final HashMap<String, SpellData> data;
	
	private SpellLanguages() {
		data = new HashMap<String, SpellData>();
	}
	
	private void parseFile(String fileName) throws IOException {
		data.clear();
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "ISO-8859-1"));
		int lineNr = 1;
		try {
			// skip first line (column headers)
			String line = in.readLine();
			lineNr++;
			line = in.readLine();
			while (line != null) {
				SpellData spellData = new SpellData(line);
				data.put(spellData.getSpellName(), spellData);
				line = in.readLine();
				lineNr++;
			}
		}
		catch (IOException e) {
			throw new IOException("Datei " + fileName + ", Zeile " + lineNr + ": " + e.getMessage());
		}
		finally {
			if (in != null) {
				in.close();
			}
 		}
	}
	
	public static SpellLanguages getInstance() {
		if (!dataRead) {
		  String dirPath = dsa.util.Directories.getApplicationPath() + "daten"
		    	        + java.io.File.separator;
		  try {
  	        dataRead = true;
		    instance.parseFile(dirPath + "Zaubersprachen.csv");
	      }
	      catch (java.io.IOException e) {
	        javax.swing.JOptionPane.showMessageDialog(null, e.getMessage(),
	            "Fehler beim Laden der Zaubersprachen", javax.swing.JOptionPane.ERROR_MESSAGE);        
	      }
	    }
	    return instance;
	}
	
	public String getSpellLanguage(String spell, Language l) {
		String res = "";
		if (data.containsKey(spell)) {
			res = data.get(spell).getTranslation(l);
		}
		return res;
	}
	
}
