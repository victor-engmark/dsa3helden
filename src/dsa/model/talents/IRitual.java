/*
    Copyright (c) 2006-2014 [Joerg Ruedenauer]
  
    This file is part of Heldenverwaltung.

    Heldenverwaltung is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Heldenverwaltung is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Heldenverwaltung; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.model.talents;

import dsa.model.DiceSpecification;
import dsa.model.characters.Property;

public interface IRitual {
	
	enum RitualType
	{
		Curse,
		Liturgy,
		SpellLike,
		Other
	}
	
	RitualType getRitualType();

	boolean isKnownAtStart();

	String getName();

	String getRequirement();

	TestData getTestData();

	LearningTestData getLearningTestData();

	public static class TestData {
		Property p1;

		Property p2;

		Property p3;

		int defaultModifier;

		boolean mHasHalfStepLess;

		public int getDefaultModifier() {
			return defaultModifier;
		}

		public boolean hasHalfStepLess() {
			return mHasHalfStepLess;
		}

		public Property getP1() {
			return p1;
		}

		public Property getP2() {
			return p2;
		}

		public Property getP3() {
			return p3;
		}

		public void setP1(Property p1) {
			this.p1 = p1;
		}

		public void setP2(Property p2) {
			this.p2 = p2;
		}

		public void setP3(Property p3) {
			this.p3 = p3;
		}

		public void setDefaultModifier(int defaultModifier) {
			this.defaultModifier = defaultModifier;
		}

		public void setHasHalfStepLess(boolean mHasHalfStepLess) {
			this.mHasHalfStepLess = mHasHalfStepLess;
		}

	}

	public static class LearningTestData extends TestData {
		DiceSpecification ap;

		DiceSpecification permanentAP;

		DiceSpecification le;

		DiceSpecification permanentLE;

		public DiceSpecification getAp() {
			return ap;
		}

		public DiceSpecification getLe() {
			return le;
		}

		public DiceSpecification getPermanentAP() {
			return permanentAP;
		}

		public DiceSpecification getPermanentLE() {
			return permanentLE;
		}

		public void setAp(DiceSpecification ap) {
			this.ap = ap;
		}

		public void setLe(DiceSpecification le) {
			this.le = le;
		}

		public void setPermanentLE(DiceSpecification permanentLE) {
			this.permanentLE = permanentLE;
		}

		public void setPermanentAP(DiceSpecification permanentAP) {
			this.permanentAP = permanentAP;
		}
		
	}

}
