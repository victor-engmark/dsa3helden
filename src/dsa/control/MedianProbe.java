/*
    Copyright (c) 2006-2014 [Joerg Ruedenauer]
  
    This file is part of Heldenverwaltung.

    Heldenverwaltung is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Heldenverwaltung is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Heldenverwaltung; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.control;

public class MedianProbe {

	public MedianProbe(int targetValue) {
		this.targetValue = targetValue;
	}
	
	public void setModifier(int value) {
		modifier = value;
	}
	
	public boolean performTest(int firstThrow, int secondThrow, int thirdThrow) {
		int outcome = performDetailedTest(firstThrow, secondThrow, thirdThrow);
		return outcome == Probe.GELUNGEN || outcome == Probe.PERFEKT;
	}
	
	public int performDetailedTest(int firstThrow, int secondThrow, int thirdThrow) {
		int median = calcMedian(firstThrow, secondThrow, thirdThrow);
		if (median == 1)
			return Probe.PERFEKT;
		else if (median == 20)
			return Probe.PATZER;
		else if (median + modifier <= targetValue)
			return Probe.GELUNGEN;
		else
			return Probe.FEHLSCHLAG;
	}
	
	public static int calcMedian(int firstThrow, int secondThrow, int thirdThrow) {
		if (firstThrow < secondThrow) {
			if (secondThrow < thirdThrow) {
				return secondThrow; // 1 < 2 < 3
			}
			else if (firstThrow < thirdThrow) {
				return thirdThrow; // 1 < 3 <= 2
			}
			else {
				return firstThrow; // 3 <= 1 < 2 
			}
		}
		else {
			if (firstThrow < thirdThrow) {
				return firstThrow; // 2 <= 1 < 3
			}
			else if (secondThrow < thirdThrow) {
				return thirdThrow; // 2 < 3 <= 1
			}
			else {
				return secondThrow; // 3 <= 2 <= 1
			}
		}
	}
	
	private int targetValue;
	private int modifier;
}
