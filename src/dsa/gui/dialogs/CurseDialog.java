/*
    Copyright (c) 2006-2014 [Joerg Ruedenauer]
  
    This file is part of Heldenverwaltung.

    Heldenverwaltung is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Heldenverwaltung is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Heldenverwaltung; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import dsa.control.Dice;
import dsa.control.MedianProbe;
import dsa.control.Probe;
import dsa.gui.lf.BGDialog;
import dsa.model.characters.Energy;
import dsa.model.characters.Group;
import dsa.model.characters.Hero;
import dsa.model.characters.Property;
import dsa.model.talents.ICurse;
import dsa.remote.RemoteManager;
import dsa.util.Strings;

public class CurseDialog extends BGDialog {
	
	private Hero hero;
	private ICurse curse;
	
	public CurseDialog(JFrame parent, Hero aHero, ICurse aCurse) {
		super(parent, true);
		hero = aHero;
		curse = aCurse;
		initialize();
	}
	
	private void initialize() {
		setTitle("Fluchen");
		setContentPane(getJContentPane());
	    this.getRootPane().setDefaultButton(getCurseButton());
	    setEscapeButton(getCancelButton());
	    getPermanentBox().setEnabled(curse.permanentAllowed());
	    updateCosts();
	    pack();
	}
	
	private JPanel contentPane;
	
	private JPanel getJContentPane() {
		if (contentPane == null) {
			contentPane = new JPanel(new BorderLayout(10, 0));
			contentPane.add(getCursePanel(), BorderLayout.CENTER);
			contentPane.add(getButtonsPanel(), BorderLayout.SOUTH);
		}
		return contentPane;
	}
	
	private JPanel cursePanel;
	
	private JPanel getCursePanel() {
		if (cursePanel == null) {
			cursePanel = new JPanel();
			BoxLayout curseLayout = new BoxLayout(cursePanel, BoxLayout.Y_AXIS);
			cursePanel.setLayout(curseLayout);
			cursePanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), 
					BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
							BorderFactory.createEmptyBorder(5, 5, 5, 5))));
			cursePanel.add(Box.createVerticalStrut(5));
			JLabel label1 = new JLabel("Fluchtechnik:");
			label1.setAlignmentX(LEFT_ALIGNMENT);
			cursePanel.add(label1);
			cursePanel.add(Box.createVerticalStrut(5));
			cursePanel.add(getTechniqueBox());
			cursePanel.add(Box.createVerticalStrut(5));
			cursePanel.add(getPermanentBox());
			cursePanel.add(Box.createVerticalStrut(5));
			cursePanel.add(getCostLabel());
			cursePanel.add(Box.createVerticalStrut(5));
		}
		return cursePanel;
	}
	
	private JComboBox techniqueBox;
	
	private JComboBox getTechniqueBox() {
		if (techniqueBox == null) {
			techniqueBox = new JComboBox();
			techniqueBox.setAlignmentX(LEFT_ALIGNMENT);
			techniqueBox.setPreferredSize(new Dimension(100, 20));
			techniqueBox.addItem("Vertrauter");
			techniqueBox.addItem("Im Zorn");
			techniqueBox.addItem("Im Schlaf");
			techniqueBox.addItem("Objekt");
			techniqueBox.setSelectedIndex(0);
			techniqueBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					updateCosts();
				}
			});
		}
		return techniqueBox;
	}
	
	private JCheckBox permanentBox;
	
	private JCheckBox getPermanentBox() {
		if (permanentBox == null) {
			permanentBox = new JCheckBox("Permanent");
			permanentBox.setAlignmentX(LEFT_ALIGNMENT);
			permanentBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					updateCosts();
				}				
			});
		}
		return permanentBox;
	}
	
	private JLabel costLabel;
	
	private JLabel getCostLabel() {
		if (costLabel == null) {
			costLabel = new JLabel("Kosten: 888 ASP");
			costLabel.setAlignmentX(LEFT_ALIGNMENT);
		}
		return costLabel;
	}
	
	private int getASPCost() {
		int techniqueIndex = getTechniqueBox().getSelectedIndex();
		return curse.getCost(ICurse.Technique.values()[techniqueIndex], permanentBox.isSelected());
	}
	
	private void curse() {
		boolean success = true;
		String text = Strings.firstWord(hero.getName()) + " spricht Fluch " + curse.getName();
		if (getTechniqueBox().getSelectedIndex() == 1) {
			// rage
			if (Group.getInstance().getOptions().use3W20Median()) {
				MedianProbe probe = new MedianProbe(hero.getCurrentProperty(Property.CH));
				probe.setModifier(hero.getMarkers());
				int firstRoll = Dice.roll(20);
				int secondRoll = Dice.roll(20);
				int thirdRoll = Dice.roll(20);
				int result = probe.performDetailedTest(firstRoll, secondRoll, thirdRoll);
				success = result == Probe.GELUNGEN || result == Probe.PERFEKT;
				if (success) {
					text += "\nCH-Probe gelungen (Wurf: " + firstRoll + ", " + secondRoll + ", " + thirdRoll + ")";
				}
				else {
					text += "\nCH-Probe nicht gelungen (Wurf: " + firstRoll + ", " + secondRoll + ", " + thirdRoll + ")";
				}
			}
			else {
				int roll = Dice.roll(20);
				success = roll + hero.getMarkers() <= hero.getCurrentProperty(Property.CH);
				if (success) {
					text += "\nCH-Probe gelungen (Wurf: " + roll + ")";
				}
				else {
					text += "\nCH-Probe nicht gelungen (Wurf: " + roll + ")";
				}
			}
		}
	    int dialogResult = ProbeResultDialog.showDialog(getParent(), text, "Fluch von "
	            + Strings.firstWord(hero.getName()), true);
    	boolean sendToServer = (dialogResult & ProbeResultDialog.SEND_TO_SINGLE) != 0;
    	boolean informOtherPlayers = (dialogResult & ProbeResultDialog.SEND_TO_ALL) != 0;
    	if (sendToServer) {
    		RemoteManager.getInstance().informOfProbe(hero, text, informOtherPlayers);
    	}
		if (success) {
			hero.changeCurrentEnergy(Energy.AE, -getASPCost());
		}
	}
	
	private void updateCosts() {
		int aspCost = getASPCost();
		getCostLabel().setText("Kosten: " + aspCost + " ASP");
	    boolean curseAllowed = aspCost <= hero.getCurrentEnergy(Energy.AE);
	    getCurseButton().setEnabled(curseAllowed);
	}
	
	private JPanel buttonsPanel;
	
	private JPanel getButtonsPanel() {
		if (buttonsPanel == null) {
			buttonsPanel = new JPanel();
			buttonsPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			BoxLayout buttonsLayout = new BoxLayout(buttonsPanel, BoxLayout.LINE_AXIS);
			buttonsPanel.setLayout(buttonsLayout);
			buttonsPanel.add(Box.createHorizontalGlue());
			buttonsPanel.add(Box.createHorizontalStrut(10));
			buttonsPanel.add(getCurseButton());
			buttonsPanel.add(Box.createHorizontalStrut(10));
			buttonsPanel.add(getCancelButton());
			buttonsPanel.add(Box.createHorizontalStrut(10));
		}
		return buttonsPanel;
	}
	
	private JButton curseButton;
	
	private JButton getCurseButton() {
		if (curseButton == null) {
			curseButton = new JButton("Fluchen!");
			curseButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					curse();
					CurseDialog.this.dispose();
				}
			});
		}
		return curseButton;
	}
	
	private JButton cancelButton;
	
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Abbrechen");
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					CurseDialog.this.dispose();
				}
			});
		}
		return cancelButton;
	}

	@Override
	public String getHelpPage() {
		return "Fluchen";
	}

}
