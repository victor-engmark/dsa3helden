/*
    Copyright (c) 2006-2008 [Joerg Ruedenauer]
  
    This file is part of Heldenverwaltung.

    Heldenverwaltung is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Heldenverwaltung is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Heldenverwaltung; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.View;

import dsa.control.Dice;
import dsa.control.Fighting;
import dsa.control.Markers;
import dsa.control.Probe;
import dsa.gui.lf.BGDialog;
import dsa.gui.util.ImageManager;
import dsa.model.DiceSpecification;
import dsa.model.characters.Energy;
import dsa.model.characters.Hero;
import dsa.model.data.SpellLanguages;
import dsa.model.data.Talents;
import dsa.model.talents.ISpellLikeAbility;
import dsa.model.talents.ISpellLikeRitual;
import dsa.model.talents.NormalTalent;
import dsa.remote.RemoteManager;
import dsa.util.Strings;

/**
 * 
 */
public final class SpellProbeDialog extends BGDialog {

  private javax.swing.JPanel jContentPane = null;

  private JButton probeButton = null;

  private JButton cancelButton = null;

  private JPanel jPanel = null;

  private JSpinner difficultySpinner = null;

  private JLabel jLabel = null;

  private Hero hero;

  private String talentName;

  private JLabel jLabel2 = null;
  
  private JLabel defaultAddLabel = null;

  private JLabel halfStepLabel = null;

  private JLabel propertyLabel = null;

  private boolean isSpell;
  
  private ISpellLikeRitual ritual = null;
  
  /**
   * This is the default constructor
   */
  public SpellProbeDialog(Frame owner, Hero hero, String talent) {
    super(owner, true);
    this.hero = hero;
    talentName = talent;
    isSpell = true;
    initialize();
  }

  public SpellProbeDialog(JFrame owner, Hero hero, ISpellLikeRitual ritual) {
	    super(owner, true);
	    this.hero = hero;
	    talentName = ritual.getName();
	    isSpell = false;
	    this.ritual = ritual;
	    initialize();
  }

  /**
   * This method initializes this
   * 
   * @return void
   */
  private void initialize() {
    this.setTitle("Probe");
    this.setContentPane(getJContentPane());
    pack();
    getDifficultySpinner().requestFocus();
    ((JSpinner.DefaultEditor) getDifficultySpinner().getEditor()).getTextField().select(
        0, 1);
    setProbability();
    this.getRootPane().setDefaultButton(getProbeButton());
    setEscapeButton(getCancelButton());
    if (!isSpell) {
        String text = "Probe geht auf: " + ritual.getTestData().getP1() + "/" + ritual.getTestData().getP2() + "/"
                + ritual.getTestData().getP3();
        propertyLabel.setText(text);
        defaultAddLabel.setText("Normaler Zuschlag für Probe: "
            + ritual.getTestData().getDefaultModifier());
        int mod = ritual.getTestData().getDefaultModifier();
        if (ritual.getTestData().hasHalfStepLess()) {
          halfStepLabel.setText("Reduzierung um halbe Stufe: -"
              + (hero.getStep() / 2));
          mod -= hero.getStep() / 2;
        }
        else {
          halfStepLabel.setText("Reduzierung um halbe Stufe: nein");
        }    	
        getDifficultySpinner().setValue(Integer.valueOf(mod));
    }
    this.getDifficultySpinner().requestFocusInWindow();
    JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor) this.getDifficultySpinner().getEditor();
    if (isSpell) {
    	editor.getTextField().setText("0");
    }
    editor.getTextField().selectAll();
  }

  /**
   * 
   * 
   */
  private void setProbability() {
    int difficulty = ((Integer) getDifficultySpinner().getModel().getValue()).intValue();
    Probe probe = new Probe();
    if (isSpell) {
	    NormalTalent talent = (NormalTalent) Talents.getInstance().getTalent(
	        talentName);
	    probe
	        .setFirstProperty(hero.getCurrentProperty(talent.getFirstProperty()));
	    probe.setSecondProperty(hero.getCurrentProperty(talent
	        .getSecondProperty()));
	    probe
	        .setThirdProperty(hero.getCurrentProperty(talent.getThirdProperty()));
	    probe.setSkill(hero.getCurrentTalentValue(talentName));
    }
    else {
        probe.setFirstProperty(hero.getCurrentProperty(ritual.getTestData().getP1()));
        probe.setSecondProperty(hero.getCurrentProperty(ritual.getTestData().getP2()));
        probe.setThirdProperty(hero.getCurrentProperty(ritual.getTestData().getP3()));
        probe.setSkill(0);
    }
    probe.setModifier(difficulty + Markers.getMarkers(hero));
    int successCount = 0;
    for (int i = 1; i <= 20; i++)
      for (int j = 1; j <= 20; j++)
        for (int k = 1; k <= 20; k++) {
          if (probe.performTest(i, j, k)) successCount++;
        }
    double successPercent = (((double) successCount) / 8000.0) * 100.0;
    String formatted = new java.text.DecimalFormat("#0.00#")
        .format(successPercent);
    jLabel2.setText("Erfolgswahrscheinlichkeit: " + formatted + "%");
    jLabel2.setForeground(successPercent >= 50.0 ? Color.GREEN : Color.RED);
  }

  /**
   * This method initializes jContentPane
   * 
   * @return javax.swing.JPanel
   */
  private javax.swing.JPanel getJContentPane() {
    if (jContentPane == null) {
      jContentPane = new javax.swing.JPanel();
      jContentPane.setLayout(new BorderLayout());
      jContentPane.add(getJPanel(), BorderLayout.NORTH);
      jContentPane.add(getButtonPane(), BorderLayout.SOUTH);
      jContentPane.add(getASPAndLanguagePane(), BorderLayout.CENTER);
    }
    return jContentPane;
  }
  
  private JPanel costPane;
  private ArrayList<JSpinner> spinners = new ArrayList<JSpinner>();
  private JComboBox variantCombo;
  private JCheckBox stabZauberCheckBox;
  private JLabel costLabel;
  
  private JPanel getASPAndLanguagePane() {
	  if (!isSpell) {
		  return getASPPane();
	  }
	  else {
		  JPanel aspAndLanguagePane = new JPanel();
		  aspAndLanguagePane.setLayout(new BorderLayout());
		  aspAndLanguagePane.add(getASPPane(), BorderLayout.NORTH);
		  aspAndLanguagePane.add(getLanguagePane(), BorderLayout.CENTER);
		  return aspAndLanguagePane;
	  }
  }
  
  JPanel aspPane;
  
  private JPanel getLanguagePane() {
	  JPanel languagePane = new JPanel();
	  languagePane.setLayout(new BoxLayout(languagePane, BoxLayout.LINE_AXIS));
	  SpellLanguages.Language language = hero.getSpellLanguage();
	  String text = "<html><body style='width: 100%'>";
	  String languageText = "";
	  if (language != SpellLanguages.Language.Keine) {
		  languageText = SpellLanguages.getInstance().getSpellLanguage(talentName, language);
		  if (languageText.equals("")) {
			  languageText = language.name() + ": " + "Unbekannt";
		  }
		  else {
			  languageText = language.name() + ": " + languageText;
		  }
	  }
	  else {
		  languageText = "Keine Lehrsprache ausgewählt (siehe Fenster 'Magie')";
	  }
	  text += languageText;
	  JLabel contentLabel = new JLabel(text);
	  JLabel measureLabel = new JLabel(text);
	  View view = (View) measureLabel.getClientProperty(
              javax.swing.plaf.basic.BasicHTML.propertyKey);
      view.setSize(aspPane.getPreferredSize().width,0);
      float w = view.getPreferredSpan(View.X_AXIS);
      float h = view.getPreferredSpan(View.Y_AXIS);	  
      contentLabel.setPreferredSize(new java.awt.Dimension((int)w, (int)h));
	  languagePane.add(contentLabel);
	  languagePane.add(Box.createHorizontalStrut(10));
	  JButton copyButton = new JButton();
	  copyButton.setIcon(ImageManager.getIcon("copy"));
	  copyButton.setToolTipText("Kopieren");
	  copyButton.addActionListener(new ActionListener() {
		  public void actionPerformed(ActionEvent e) {
			  SpellLanguages.Language l = hero.getSpellLanguage();
  			  Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			  c.setContents(new StringSelection(SpellLanguages.getInstance().getSpellLanguage(talentName, l)), new ClipboardOwner() {
				  public void lostOwnership(Clipboard clipboard, Transferable contents) {
				  }
			  });
		  }
	  });
	  if (language == SpellLanguages.Language.Keine || SpellLanguages.getInstance().getSpellLanguage(talentName, language).equals("")) {
		  copyButton.setEnabled(false);
	  }
	  languagePane.add(copyButton);
	  languagePane.setBorder(javax.swing.BorderFactory.createTitledBorder(
			  javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.LOWERED), "Zauber in Lehrsprache"));
	  return languagePane;
  }
  
  private JPanel getASPPane() {
    aspPane = new JPanel();
    aspPane.setLayout(new BorderLayout());
    ISpellLikeAbility spell = isSpell ? (ISpellLikeAbility) Talents.getInstance().getTalent(talentName) : ritual;
    if (spell.getNrOfVariants() > 1) {
      JPanel variantPanel = new JPanel();
      variantPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
      variantPanel.add(new JLabel("Variante: "));
      variantCombo = new JComboBox();
      for (int i = 0; i < spell.getNrOfVariants(); ++i) {
        variantCombo.addItem(spell.getVariantName(i));
      }
      variantCombo.setSelectedIndex(0);
      variantCombo.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          fillCostPane(variantCombo.getSelectedIndex()); 
          costPane.invalidate();
          SpellProbeDialog.this.pack();
        }
      });
      variantPanel.add(variantCombo);
      aspPane.add(variantPanel, BorderLayout.NORTH);
    }
    if (isSpell) {
	    stabZauberCheckBox = new JCheckBox("4. Stabzauber benutzen");
	    stabZauberCheckBox.setEnabled(false);
	    if (hero.getInternalType().startsWith("Magier")) {
	      List<String> rituals = hero.getRituals();
	      for (int i = 0; i < rituals.size(); ++i) {
	        if (rituals.get(i).equals("4. Stabzauber")) {
	          if (!Fighting.Flammenschwert1.equals(hero.getFirstHandWeapon()) && !Fighting.Flammenschwert2.equals(hero.getFirstHandWeapon())) {
		          stabZauberCheckBox.setEnabled(true);
		          stabZauberCheckBox.setSelected(true);
		          stabZauberCheckBox.addActionListener(new ActionListener() {
		            public void actionPerformed(ActionEvent e) {
		              calcCosts();
		            }
		          });
	          }
	        }
	      }
	    }
    }
    JPanel costWrapper = new JPanel(new FlowLayout(FlowLayout.LEFT));
    costLabel = new JLabel();
    costWrapper.add(costLabel);
    costPane = new JPanel();
    fillCostPane(0);
    aspPane.add(costPane, BorderLayout.CENTER);
    if (isSpell) {
        JPanel lowerPane = new JPanel(new GridLayout(2, 1));
    	JPanel cbWrapper = new JPanel(new FlowLayout(FlowLayout.LEFT));
    	cbWrapper.add(stabZauberCheckBox);
        lowerPane.add(cbWrapper);
        lowerPane.add(costWrapper);
        aspPane.add(lowerPane, BorderLayout.SOUTH);
    }
    else {
        aspPane.add(costWrapper, BorderLayout.SOUTH);
    }
    aspPane.setBorder(javax.swing.BorderFactory
        .createTitledBorder(javax.swing.BorderFactory
        .createEtchedBorder(javax.swing.border.EtchedBorder.LOWERED), "Kosten"));
    return aspPane;
  }
  
  int aeSum = 0;
  int leSum = 0;
  int permanentSum = 0;
  String costInfo = "";
  
  private static void addCosts(ArrayList<DiceSpecification> costs, DiceSpecification cost) {
	  if (cost.getNrOfDices() == 0 && cost.getDiceSize() != 0) {
		  cost = DiceSpecification.create(0, 0, cost.getFixedPoints());
	  }
	  if (costs.isEmpty()) {
		  if (cost.getNrOfDices() > 0) {
			  costs.add(DiceSpecification.create(cost.getNrOfDices(), cost.getDiceSize(), 0));
		  }
		  costs.add(DiceSpecification.create(0, 0, cost.getFixedPoints()));
		  return;
	  }
	  for (int i = 0; i < costs.size(); ++i) {
		  if (costs.get(i).getDiceSize() == cost.getDiceSize()) {
			  if (cost.getDiceSize() == 0) {
				  DiceSpecification newSpec = DiceSpecification.create(0, 0, cost.getFixedPoints() + costs.get(i).getFixedPoints());
				  costs.remove(i);
				  costs.add(newSpec);
				  return;
			  }
			  else {
				  DiceSpecification newSpec = DiceSpecification.create(costs.get(i).getNrOfDices() + cost.getNrOfDices(), cost.getDiceSize(), 0);
				  costs.remove(i);
				  costs.add(i, newSpec);
				  break;
			  }
		  }
		  else if (costs.get(i).getDiceSize() < cost.getDiceSize()) {
			  costs.add(i, DiceSpecification.create(cost.getNrOfDices(), cost.getDiceSize(), 0));
			  break;
		  }
	  }
	  DiceSpecification newFixed = DiceSpecification.create(0,  0,  cost.getFixedPoints() + costs.get(costs.size() - 1).getFixedPoints());
	  costs.remove(costs.size() - 1);
	  costs.add(newFixed);
  }
  
  private static String PrintCosts(ArrayList<DiceSpecification> overallCosts, String unit) {
	  String text = "";
	  if (overallCosts.size() == 1 && overallCosts.get(0).getFixedPoints() <= 0) {
		  text = "1";
	  }
	  else {
	      for (int i = 0; i < overallCosts.size(); ++i) {
	    	  if (overallCosts.get(i).getNrOfDices() == 0 && overallCosts.get(i).getFixedPoints() == 0)
	    		  continue;
	    	  if (i > 0 && ((overallCosts.get(i).getNrOfDices() > 0) || overallCosts.get(i).getFixedPoints() > 0)) text +=  " + ";
	    	  text += overallCosts.get(i).toString();
	      }
	  }
      text += " " + unit;
      return text;
  }

  private void calcCosts() {
    aeSum = 0;
    leSum = 0;
    permanentSum = 0;
    costInfo = "";
    int variant = 0;
    ISpellLikeAbility spell = isSpell ? (ISpellLikeAbility) Talents.getInstance().getTalent(talentName) : ritual;
    if (spell.getNrOfVariants() > 1) {
    	variant = variantCombo.getSelectedIndex();
    	costInfo = "Variante: " + variantCombo.getSelectedItem() + "; ";
    }
    List<ISpellLikeAbility.Cost> costs = spell.getCosts(variant);
    int spinnerIndex = 0;
    ArrayList<DiceSpecification> overallCosts = new ArrayList<DiceSpecification>();
    ArrayList<DiceSpecification> overallPermanentCosts = new ArrayList<DiceSpecification>();
    ArrayList<DiceSpecification> overallLECosts = new ArrayList<DiceSpecification>();
    for (int i = 0; i < costs.size(); ++i) {
      ISpellLikeAbility.Cost cost = costs.get(i);
      if (cost.getCostType() == ISpellLikeAbility.CostType.Custom) {
        int val = getSpinnerValue(spinnerIndex); ++spinnerIndex;
        if (val < cost.getMinimumCost()) val = cost.getMinimumCost();
        aeSum += val;
        if (i > 0) costInfo += "; ";
        costInfo += "Frei bestimmte Kosten: " + val + " ASP"; 
        addCosts(overallCosts, DiceSpecification.create(0, 0, val));
      }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.Fixed) {
        int val= cost.getCost().calcValue();
        if (val < cost.getMinimumCost()) val = cost.getMinimumCost();
        aeSum += val;
        if (i > 0) costInfo += "; ";
        if (cost.getCost().getNrOfDices() > 0) {
        	costInfo += "Feste Kosten: " + cost.getCost();
        	if (cost.getMinimumCost() > 0) costInfo += " (Min. " + cost.getMinimumCost() + ")";
        	costInfo += " ASP";
        }
        else {
        	costInfo += "Feste Kosten: " + val + " ASP";
        }
        addCosts(overallCosts, cost.getCost());
      }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.LP) {
        int val = cost.getCost().calcValue();
        if (val < cost.getMinimumCost()) val = cost.getMinimumCost();
        leSum += val;
        if (i > 0) costInfo += "; ";
        costInfo += "LE-Kosten: " + cost.getCost();
        addCosts(overallLECosts, cost.getCost());
      }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.Multiplied) {
        int val1 = getSpinnerValue(spinnerIndex);
        int val2 = cost.getCost().calcValue(); 
        int val = val1 * val2;
        if (val < cost.getMinimumCost()) val = cost.getMinimumCost();
        ++spinnerIndex;
        aeSum += val;
        if (i > 0) costInfo += "; ";
        costInfo += "Kosten: " + val1 + " (" + cost.getText() + ") mal " + val2;
    	if (cost.getMinimumCost() > 0) costInfo += " (Min. " + cost.getMinimumCost() + ")";
    	costInfo += " ASP";
    	if (cost.getCost().getNrOfDices() == 0) {
    		costInfo += " = " + val + " ASP";
    		addCosts(overallCosts, DiceSpecification.create(0, 0, val));
    	}
    	else for (int j = 0; j < val1; ++j){
    		addCosts(overallCosts, cost.getCost());
    	}
      }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.Permanent) {
        int val = cost.getCost().calcValue();
        if (val < cost.getMinimumCost()) val = cost.getMinimumCost();
        permanentSum += val;
        if (i > 0) costInfo += "; ";
        costInfo += "Permanente Kosten: " + cost.getCost() + " ASP";
        addCosts(overallPermanentCosts, cost.getCost());
      }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.Quadratic) {
        int value = getSpinnerValue(spinnerIndex); ++spinnerIndex;
        int val = value * value;
        if (val < cost.getMinimumCost()) val = cost.getMinimumCost();
        aeSum += val;
        if (i > 0) costInfo += "; ";
        costInfo += "Kosten: " + value + " (" + cost.getText() + ") zum Quadrat";
    	if (cost.getMinimumCost() > 0) costInfo += " (Min. " + cost.getMinimumCost() + ")";
        costInfo += " ASP = " + val + " ASP";
        addCosts(overallCosts, DiceSpecification.create(0,  0,  val));
      }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.Special) {
        // nothing
        if (i > 0) costInfo += "; ";
    	costInfo += "Spezielle Kosten, ASP müssen manuell abgezogen werden.";
      }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.Step) {
        int val = hero.getStep();
        if (val < cost.getMinimumCost()) val = cost.getMinimumCost();
        aeSum += val;
        if (i > 0) costInfo += "; ";
        costInfo += "Kosten: Stufe";
    	if (cost.getMinimumCost() > 0) costInfo += " (Min. " + cost.getMinimumCost() + ")";
    	costInfo += " ASP = " + val + " ASP";
    	addCosts(overallCosts, DiceSpecification.create(0,  0,  val));
      }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.Variable) {
        int value = getSpinnerValue(spinnerIndex); ++spinnerIndex;
        int sum = 0;
        for (int j = 0; j < value; ++j) {
          sum += cost.getCost().calcValue();
        }
        if (sum < cost.getMinimumCost()) sum = cost.getMinimumCost();
        aeSum += sum;
        if (i > 0) costInfo += "; ";
        costInfo += "Kosten: " + value + " (" + cost.getText() + ") mal " + cost.getCost();
    	if (cost.getMinimumCost() > 0) costInfo += " (Min. " + cost.getMinimumCost() + ")";
    	costInfo += " ASP";
        if (cost.getCost().getNrOfDices() == 0) costInfo += " = " + sum + " ASP";
        for (int j = 0; j < value; ++j) {
      	  addCosts(overallCosts, cost.getCost());
        }
     }
      else if (cost.getCostType() == ISpellLikeAbility.CostType.Dice) {
          int value = getSpinnerValue(spinnerIndex); ++spinnerIndex;
          int sum = 0;
          for (int j = 0; j < value; ++j) {
            sum += cost.getCost().calcValue();
          }
          if (sum < cost.getMinimumCost()) sum = cost.getMinimumCost();
          aeSum += sum;
          if (i > 0) costInfo += "; ";
          costInfo += "Kosten: " + value + " mal " + cost.getCost();
          if (cost.getMinimumCost() > 0) costInfo += " (Min. " + cost.getMinimumCost() + ")";
      	  costInfo += " ASP"; 
          if (cost.getCost().getNrOfDices() == 0) costInfo += " = " + sum + " ASP";
          for (int j = 0; j < value; ++j) {
        	  addCosts(overallCosts, cost.getCost());
          }
      }
    }
    if (isSpell && stabZauberCheckBox.isSelected()) {
    	aeSum -= 2;
    	addCosts(overallCosts, DiceSpecification.create(0, 0, -2));
    }
    if (aeSum <= 0) aeSum = 1;
    String text = "Gesamtkosten: ";
    text += PrintCosts(overallCosts, "ASP");
    if (overallPermanentCosts.size() > 0) {
      text = text + " und " + PrintCosts(overallPermanentCosts, "permanente ASP");
    }
    if (overallLECosts.size() > 0) {
      text = text + " und " + PrintCosts(overallLECosts, "LP");
    }
    text = text + ".";
    costLabel.setText(text);
    boolean ok = true;
    if (aeSum + permanentSum > hero.getCurrentEnergy(Energy.AE)) ok = false;
    if (leSum > hero.getCurrentEnergy(Energy.LE)) ok = false;
    if (permanentSum > hero.getDefaultEnergy(Energy.AE)) ok = false;
    costLabel.setForeground(ok ? Color.GREEN : Color.RED);
    probeButton.setEnabled(ok);
  }
  
  private int getSpinnerValue(int index) {
    return ((SpinnerNumberModel)spinners.get(index).getModel()).getNumber().intValue();
  }
  
  private void fillCostPane(int variant) {
    costPane.removeAll();
    spinners.clear();
    ISpellLikeAbility spell = isSpell ? (ISpellLikeAbility) Talents.getInstance().getTalent(talentName) : ritual;
    int nrOfRows = 0;
    List<ISpellLikeAbility.Cost> costs = spell.getCosts(variant); 
    for (int i = 0; i < costs.size(); ++i) {
      ISpellLikeAbility.CostType ct = costs.get(i).getCostType();
      if (ct == ISpellLikeAbility.CostType.Custom || ct == ISpellLikeAbility.CostType.Multiplied 
          || ct == ISpellLikeAbility.CostType.Quadratic || ct == ISpellLikeAbility.CostType.Variable || ct == ISpellLikeAbility.CostType.Dice) {
        nrOfRows += 2;
      }
      else nrOfRows += 1;
    }
    costPane.setLayout(new GridLayout(nrOfRows, 1));
    String intro = isSpell ? "Der Zauber " : "Das Ritual ";
    for (int i = 0; i < costs.size(); ++i) {
      ISpellLikeAbility.CostType ct = costs.get(i).getCostType();
      if (ct == ISpellLikeAbility.CostType.Fixed) {
        addText(intro + "hat feste Kosten von " + costs.get(i).getCost()
        		+ (costs.get(i).getMinimumCost() > 0 ? (" (Min. " + costs.get(i).getMinimumCost() + ")") : "")
        		+ " ASP.");
      }
      else if (ct == ISpellLikeAbility.CostType.Permanent) {
        addText(intro + "hat permanente Kosten von " + costs.get(i).getCost() 
        		+ (costs.get(i).getMinimumCost() > 0 ? (" (Min. " + costs.get(i).getMinimumCost() + ")") : "")
        		+ " ASP.");
      }
      else if (ct == ISpellLikeAbility.CostType.LP) {
        addText(intro + "hat feste Kosten von " + costs.get(i).getCost() 
        		+ (costs.get(i).getMinimumCost() > 0 ? (" (Min. " + costs.get(i).getMinimumCost() + ")") : "")
        		+ " LP.");
      }
      else if (ct == ISpellLikeAbility.CostType.Special) {
        addText(intro + "hat spezielle Kosten. Bitte manuell die Energien anpassen.");
      }
      else if (ct == ISpellLikeAbility.CostType.Step) {
        addText(intro + "hat feste Kosten von " + hero.getStep() + " (Stufe)"
        		+ (costs.get(i).getMinimumCost() > 0 ? (" (Min. " + costs.get(i).getMinimumCost() + ")") : "")
        		+ " ASP.");
      }
      else if (ct == ISpellLikeAbility.CostType.Custom) {
        addText(intro + "hat frei bestimmbare Kosten.");
        addVariablePane("Bitte Kosten angeben: ", costs.get(i).getCost().calcValue());
      }
      else if (ct == ISpellLikeAbility.CostType.Multiplied) {
        addText(intro + "kostet " + costs.get(i).getCost() + " mal " + costs.get(i).getText() 
        		+ (costs.get(i).getMinimumCost() > 0 ? (" (Min. " + costs.get(i).getMinimumCost() + ")") : "")
        		+ " ASP.");
        addVariablePane(costs.get(i).getText() + ": ", 1);
      }
      else if (ct == ISpellLikeAbility.CostType.Quadratic) {
        addText(intro + "kostet " + costs.get(i).getText() + " zum Quadrat"
        		+ (costs.get(i).getMinimumCost() > 0 ? (" (Min. " + costs.get(i).getMinimumCost() + ")") : "")
        		+ " ASP.");
        addVariablePane(costs.get(i).getText() + ": ", 1);        
      }
      else if (ct == ISpellLikeAbility.CostType.Variable) {
        addText(intro + "kostet " + costs.get(i).getCost() + " ASP pro " + costs.get(i).getText() 
        		+ (costs.get(i).getMinimumCost() > 0 ? (" (Min. " + costs.get(i).getMinimumCost() + ")") : "")
        		+ ".");
        addVariablePane(costs.get(i).getText() + ": ", 0);
      }
      else if (ct == ISpellLikeAbility.CostType.Dice) {
    	  addText(intro + "kostet auswählbar viele " + costs.get(i).getCost()
    	  	+ (costs.get(i).getMinimumCost() > 0 ? (" (Min. " + costs.get(i).getMinimumCost() + ")") : "")
    	  	+ " ASP.");
    	  addVariablePane(costs.get(i).getCost() + ": ", 1, hero.getStep() + 1);
      }
    }
    calcCosts();
  }
  
  private void addText(String text) {
    JPanel pane = new JPanel();
    pane.setLayout(new FlowLayout(FlowLayout.LEFT));
    JLabel l = new JLabel();
    l.setText(text);
    pane.add(l);
    costPane.add(pane);
  }
  
  private void addVariablePane(String text, int minimum) {
	addVariablePane(text, minimum, 150);
  }
  
  private void addVariablePane(String text, int minimum, int maximum) {
    JPanel pane = new JPanel();
    pane.setLayout(new FlowLayout(FlowLayout.LEFT));
    JLabel l = new JLabel();
    l.setText(text);
    pane.add(l);
    JSpinner spinner = new JSpinner(new SpinnerNumberModel(minimum == 0 ? 1 : minimum, minimum, maximum, 1));
    spinner.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        calcCosts();
      }
    });
    spinners.add(spinner);
    pane.add(spinner);
    costPane.add(pane);
  }
  
  private JPanel getButtonPane() {
    JPanel buttonPane = new JPanel();
    buttonPane.setPreferredSize(new java.awt.Dimension(250, 55));
    buttonPane.setLayout(null);
    buttonPane.add(getProbeButton(), null);
    buttonPane.add(getCancelButton(), null);
    return buttonPane;
  }

  /**
   * This method initializes jButton
   * 
   * @return javax.swing.JButton
   */
  private JButton getProbeButton() {
    if (probeButton == null) {
      probeButton = new JButton();
      probeButton.setName("");
      probeButton.setText("Probe!");
      probeButton.setMnemonic(java.awt.event.KeyEvent.VK_P);
      probeButton.setPreferredSize(new java.awt.Dimension(90, 25));
      probeButton.setLocation(33, 15);
      probeButton.setSize(90, 25);
      probeButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          doProbe();
        }
      });
    }
    return probeButton;
  }

  protected void doProbe() {
    int difficulty = ((Integer) getDifficultySpinner().getModel().getValue()).intValue();
    java.awt.Container parent = getParent();
    dispose();
    String result = talentName;
	if (difficulty > 0)
		result += " +" + difficulty;
	else if (difficulty < 0)
		result += " " + difficulty;
	result += ": ";
    result += doProbe(hero, talentName, difficulty);
    result += "\nNoch " + hero.getCurrentEnergy(Energy.AE) + " ASP übrig.";
    int dialogResult = ProbeResultDialog.showDialog(parent, result, "Probe für "
        + Strings.firstWord(hero.getName()), true);
	boolean sendToServer = (dialogResult & ProbeResultDialog.SEND_TO_SINGLE) != 0;
	boolean informOtherPlayers = (dialogResult & ProbeResultDialog.SEND_TO_ALL) != 0;
	if (sendToServer) {
		RemoteManager.getInstance().informOfProbe(hero, result, informOtherPlayers);
	}
  }

  private String doProbe(Hero character, String talentName, int mod) {
    Probe probe = new Probe();
    if (isSpell) {
	    NormalTalent talent = (NormalTalent) Talents.getInstance().getTalent(
	        talentName);
	    probe.setFirstProperty(character.getCurrentProperty(talent
	        .getFirstProperty()));
	    probe.setSecondProperty(character.getCurrentProperty(talent
	        .getSecondProperty()));
	    probe.setThirdProperty(character.getCurrentProperty(talent
	        .getThirdProperty()));
	    probe.setSkill(character.getCurrentTalentValue(talentName));
    }
    else {
        probe.setFirstProperty(character.getCurrentProperty(ritual.getTestData().getP1()));
        probe.setSecondProperty(character.getCurrentProperty(ritual.getTestData().getP2()));
        probe.setThirdProperty(character.getCurrentProperty(ritual.getTestData().getP3()));
        probe.setSkill(0);    	
    }
    mod += Markers.getMarkers(character);
    probe.setModifier(mod);
    int throw1 = Dice.roll(20);
    int throw2 = Dice.roll(20);
    int throw3 = Dice.roll(20);
    int result = probe.performDetailedTest(throw1, throw2, throw3);
    String s = "\n (Wurf: " + throw1 + ", " + throw2 + ", " + throw3 + ")";
    if (result == Probe.DAEMONENPECH) {
      String costs = removeCosts(false);
      return "DÄMONISCHES PECH! 3x die 20!" + "\n" + costs;
    }
    else if (result == Probe.PATZER) {
    	String costs = removeCosts(false);
      return "Patzer (2 20er)!" + s + "\n" + costs;
    }
    else if (result == Probe.PERFEKT) {
    	String costs = removeCosts(true);
      return "Perfekt (2 1er)!" + s + "\n" + costs;
    }
    else if (result == Probe.GOETTERGLUECK) {
    	String costs = removeCosts(true);
      return "GÖTTLICHE GUNST! 3x die 1!" + "\n" + costs;
    }
    else if (result == Probe.FEHLSCHLAG) {
    	String costs = removeCosts(false);
      return "Nicht gelungen." + s + "\n" + costs;
    }
    else {
    	String costs = removeCosts(true);
      return "Gelungen; " + result + (result == 1 ? " Punkt" : " Punkte")
          + " übrig." + s + "\n" + costs;
    }
  }
  
  private String removeCosts(boolean success) {
    int aeCosts = success ? aeSum : (int) Math.floor(((double)aeSum) / 2.0);
    int leCosts = success ? leSum : (int) Math.floor(((double)leSum) / 2.0);
    if (aeCosts < 1) aeCosts = 1;
    int permanentCosts = success ? permanentSum : 0;
    String costs = costInfo;
    costs += ".\nTatsächliche Kosten: " + aeCosts + " ASP";
    if (leCosts > 0) costs += ", " + leCosts + " LEP";
    if (permanentCosts > 0) costs += ", " + permanentCosts + " permanente ASP";
    costs += ".";
    hero.changeCurrentEnergy(Energy.AE, -aeCosts);
    if (leCosts > 0) hero.changeCurrentEnergy(Energy.LE, -leCosts);
    if (permanentCosts > 0) hero.changeDefaultEnergy(Energy.AE, -permanentCosts);
    return costs;
  }

  /**
   * This method initializes jButton1
   * 
   * @return javax.swing.JButton
   */
  private JButton getCancelButton() {
    if (cancelButton == null) {
      cancelButton = new JButton();
      cancelButton.setPreferredSize(new java.awt.Dimension(90, 25));
      cancelButton.setText("Abbruch");
      cancelButton.setMnemonic(java.awt.event.KeyEvent.VK_A);
      cancelButton.setLocation(148, 15);
      cancelButton.setSize(90, 25);
      cancelButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          SpellProbeDialog.this.dispose();
        }
      });
    }
    return cancelButton;
  }

  /**
   * This method initializes jPanel
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getJPanel() {
    if (jPanel == null) {
      jLabel2 = new JLabel();
      jPanel = new JPanel();
      jLabel = new JLabel();
      jPanel.setLayout(null);
      if (isSpell) {
	      jPanel.setPreferredSize(new java.awt.Dimension(247, 70));
	      jPanel.setBorder(javax.swing.BorderFactory
	          .createTitledBorder(javax.swing.BorderFactory
	          .createEtchedBorder(javax.swing.border.EtchedBorder.LOWERED), "Schwierigkeit"));
	      jLabel.setBounds(12, 15, 62, 25);
	      jLabel.setText("Zuschlag:");
	      // jLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
	      jPanel.add(getDifficultySpinner(), null);
	      jPanel.add(jLabel, null);
	      jLabel.setLabelFor(getDifficultySpinner());
	      jLabel2.setBounds(12, 42, 224, 20);
	      jLabel2.setText("Erfolgswahrscheinlichkeit");
	      jLabel2.setForeground(java.awt.Color.RED);
	      jPanel.add(jLabel2, null);
      }
      else {
          propertyLabel = new JLabel();
          propertyLabel.setBounds(new java.awt.Rectangle(12, 44, 222, 19));
          propertyLabel.setText("Probe geht auf:");
          halfStepLabel = new JLabel();
          halfStepLabel.setBounds(new java.awt.Rectangle(11, 125, 226, 18));
          halfStepLabel.setText("Reduzierung um halbe Stufe: ");
          defaultAddLabel = new JLabel();
          defaultAddLabel.setBounds(new java.awt.Rectangle(12, 97, 224, 19));
          defaultAddLabel.setText("Normaler Zuschlag für Probe: ");
          //jPanel.setBounds(12, 42, 247, 152);
          jPanel.setPreferredSize(new java.awt.Dimension(247, 152));
	      jPanel.setBorder(javax.swing.BorderFactory
		          .createTitledBorder(javax.swing.BorderFactory
		          .createEtchedBorder(javax.swing.border.EtchedBorder.LOWERED), "Schwierigkeit"));
          jLabel.setBounds(12, 15, 62, 25);
          jLabel.setText("Zuschlag:");
          // jLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
          jPanel.add(getDifficultySpinner(), null);
          jPanel.add(jLabel, null);
          jLabel.setLabelFor(getDifficultySpinner());
          jLabel2.setBounds(11, 69, 224, 20);
          jLabel2.setText("Erfolgswahrscheinlichkeit");
          jLabel2.setForeground(java.awt.Color.RED);
          jPanel.add(jLabel2, null);
          jPanel.add(defaultAddLabel, null);
          jPanel.add(halfStepLabel, null);
          jPanel.add(propertyLabel, null);
      }
    }
    return jPanel;
  }

  /**
   * This method initializes jSpinner
   * 
   * @return javax.swing.JSpinner
   */
  private JSpinner getDifficultySpinner() {
    if (difficultySpinner == null) {
      difficultySpinner = new JSpinner();
      difficultySpinner.setSize(43, 20);
      difficultySpinner.setLocation(81, 18);
      difficultySpinner.setModel(new SpinnerNumberModel(0, -50, 80, 1));
      difficultySpinner.addChangeListener(new ChangeListener() {
        public void stateChanged(ChangeEvent e) {
          setProbability();
        }
      });
    }
    return difficultySpinner;
  }

  public String getHelpPage() {
    return "Probe";
  }
} //  @jve:decl-index=0:visual-constraint="10,10"
