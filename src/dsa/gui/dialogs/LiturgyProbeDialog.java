/*
    Copyright (c) 2006-2008 [Joerg Ruedenauer]
  
    This file is part of Heldenverwaltung.

    Heldenverwaltung is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Heldenverwaltung is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Heldenverwaltung; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package dsa.gui.dialogs;

import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsa.control.Dice;
import dsa.control.Markers;
import dsa.control.Probe;
import dsa.gui.lf.BGDialog;
import dsa.model.characters.Energy;
import dsa.model.characters.Hero;
import dsa.model.talents.ILiturgy;
import dsa.model.talents.IRitual;
import dsa.remote.RemoteManager;
import dsa.util.Strings;

/**
 * 
 */
public final class LiturgyProbeDialog extends BGDialog {

  private javax.swing.JPanel jContentPane = null;

  private JButton jButton = null;

  private JButton cancelButton = null;

  private JPanel jPanel = null;

  private JSpinner difficultySpinner = null;

  private JLabel jLabel = null;

  private Hero hero;

  private ILiturgy liturgy;

  private JLabel jLabel2 = null;

  /**
   * This is the default constructor
   */
  public LiturgyProbeDialog(Frame owner, Hero hero, ILiturgy ritual) {
    super(owner, true);
    this.hero = hero;
    liturgy = ritual;
    initialize();
  }

  /**
   * This method initializes this
   * 
   * @return void
   */
  private void initialize() {
    this.setTitle("Probe");
    this.setSize(320, 340);
    this.setContentPane(getJContentPane());

    String text = "Probe geht auf: " + liturgy.getTestData().getP1() + "/" + liturgy.getTestData().getP2() + "/"
        + liturgy.getTestData().getP3();
    propertyLabel.setText(text);
    defaultAddLabel.setText("Normaler Zuschlag für Probe: "
        + getModifier(liturgy.getDegree()));
    int mod = liturgy.getTestData().getDefaultModifier();
    if (liturgy.getTestData().hasHalfStepLess()) {
      halfStepLabel.setText("Reduzierung um halbe Stufe: -"
          + (hero.getStep() / 2));
      mod -= hero.getStep() / 2;
    }
    else {
      halfStepLabel.setText("Reduzierung um halbe Stufe: nein");
    }
    getDifficultySpinner().setValue(Integer.valueOf(mod));

    getDifficultySpinner().requestFocus();
    ((JSpinner.DefaultEditor) getDifficultySpinner().getEditor()).getTextField().select(
        0, 1);
    setProbability();
    this.getRootPane().setDefaultButton(getProbeButton());
    setEscapeButton(getCancelButton());
	boolean probeAllowed = getKECost(getRealDegree()) <= hero.getCurrentEnergy(Energy.KE);
	getProbeButton().setEnabled(probeAllowed);
    this.getDifficultySpinner().requestFocusInWindow();
    JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor) this.getDifficultySpinner().getEditor();
    editor.getTextField().setText(editor.getTextField().getText());
    editor.getTextField().selectAll();
  }

  /**
   * 
   * 
   */
  private void setProbability() {
    int difficulty = ((Integer) getDifficultySpinner().getModel().getValue()).intValue();
    Probe probe = new Probe();
    probe.setFirstProperty(hero.getCurrentProperty(liturgy.getTestData().getP1()));
    probe.setSecondProperty(hero.getCurrentProperty(liturgy.getTestData().getP2()));
    probe.setThirdProperty(hero.getCurrentProperty(liturgy.getTestData().getP3()));
    probe.setSkill(0);
    probe.setModifier(difficulty + Markers.getMarkers(hero));
    int successCount = 0;
    for (int i = 1; i <= 20; i++)
      for (int j = 1; j <= 20; j++)
        for (int k = 1; k <= 20; k++) {
          if (probe.performTest(i, j, k)) successCount++;
        }
    double successPercent = (((double) successCount) / 8000.0) * 100.0;
    String formatted = new java.text.DecimalFormat("#0.00#")
        .format(successPercent);
    jLabel2.setText("Erfolgswahrscheinlichkeit: " + formatted + "%");
  }

  /**
   * This method initializes jContentPane
   * 
   * @return javax.swing.JPanel
   */
  private javax.swing.JPanel getJContentPane() {
    if (jContentPane == null) {
      jContentPane = new javax.swing.JPanel();
      jContentPane.setLayout(null);
      jContentPane.add(getProbeButton(), null);
      jContentPane.add(getCancelButton(), null);
      jContentPane.add(getJPanel(), null);
    }
    return jContentPane;
  }

  /**
   * This method initializes jButton
   * 
   * @return javax.swing.JButton
   */
  private JButton getProbeButton() {
    if (jButton == null) {
      jButton = new JButton();
      jButton.setName("");
      jButton.setText("Probe!");
      jButton.setMnemonic(java.awt.event.KeyEvent.VK_P);
      jButton.setPreferredSize(new java.awt.Dimension(90, 25));
      jButton.setLocation(60, 270);
      jButton.setSize(90, 25);
      jButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          doProbe();
        }
      });
    }
    return jButton;
  }

  protected void doProbe() {
    int difficulty = ((Integer) getDifficultySpinner().getModel().getValue()).intValue();
    java.awt.Container parent = getParent();
    dispose();
    String ret = liturgy.getName() + (difficulty > 0 ? " +" + difficulty : " " + difficulty) + ":\n";
    ret += doProbe(hero, liturgy.getTestData(), difficulty);
    int dialogResult = ProbeResultDialog.showDialog(parent, ret, "Ritual-Probe für "
        + Strings.firstWord(hero.getName()), true);
	boolean sendToServer = (dialogResult & ProbeResultDialog.SEND_TO_SINGLE) != 0;
	boolean informOtherPlayers = (dialogResult & ProbeResultDialog.SEND_TO_ALL) != 0;
	if (sendToServer) {
		RemoteManager.getInstance().informOfProbe(hero, ret, informOtherPlayers);
	}
  }

  public enum Result {
    Success, Failure, Canceled
  };

  private Result result = Result.Canceled;

  private JLabel defaultAddLabel = null;

  private JLabel halfStepLabel = null;

  private JLabel propertyLabel = null;
  
  private JLabel modifiersLabel = null;
  
  private JCheckBox increaseBox = null;
  
  private JLabel costLabel = null;

  public Result getResult() {
    return result;
  }

  private String doProbe(Hero character, IRitual.TestData test, int mod) {
    Probe probe = new Probe();
    probe.setFirstProperty(character.getCurrentProperty(test.getP1()));
    probe.setSecondProperty(character.getCurrentProperty(test.getP2()));
    probe.setThirdProperty(character.getCurrentProperty(test.getP3()));
    probe.setSkill(0);
    probe.setModifier(mod + Markers.getMarkers(character));
    int throw1 = Dice.roll(20);
    int throw2 = Dice.roll(20);
    int throw3 = Dice.roll(20);
    int ret = probe.performDetailedTest(throw1, throw2, throw3);
    String s = "\n (Wurf: " + throw1 + ", " + throw2 + ", " + throw3 + ")";
    String costs = "";
    if (ret == Probe.DAEMONENPECH) {
      this.result = Result.Failure;
      costs = removeCosts(false);
      return "DÄMONISCHES PECH! 3x die 20!" + costs;
    }
    else if (ret == Probe.PATZER) {
      this.result = Result.Failure;
      costs = removeCosts(false);
      return "Patzer (2 20er)!" + s + costs;
    }
    else if (ret == Probe.PERFEKT) {
      this.result = Result.Success;
      costs = removeCosts(true);
      return "Perfekt (2 1er)!" + s + costs;
    }
    else if (ret == Probe.GOETTERGLUECK) {
      this.result = Result.Success;
      costs = removeCosts(true);
      return "GÖTTLICHE GUNST! 3x die 1!" + costs;
    }
    else if (ret == Probe.FEHLSCHLAG) {
      this.result = Result.Failure;
      costs = removeCosts(false);
      return "Nicht gelungen." + s + costs;
    }
    else {
      this.result = Result.Success;
      costs = removeCosts(true);
      return "Gelungen; " + ret + (ret == 1 ? " Punkt" : " Punkte")
          + " übrig." + s + costs;
    }
  }
  
  private String removeCosts(boolean success) {
	  int keCost = getKECost(getRealDegree());
	  if (!success) {
		  keCost = (int)Math.ceil((double)keCost / 10.0);
	  }
	  String costs = "\nKosten: " + keCost + " KP.";
	  int permanentCost = success ? getPermanentKECost(getRealDegree()) : 0;
	  if (permanentCost > 0) {
		  keCost -= permanentCost;
		  costs += "\nPermanente Kosten: " + permanentCost + " KP.";
	  }
	  if (keCost > 0) {
		  hero.changeCurrentEnergy(Energy.KE, -keCost);
	  }
	  if (permanentCost > 0) {
		  hero.changeDefaultEnergy(Energy.KE, -permanentCost);
	  }
	  costs += "\nNoch " + hero.getCurrentEnergy(Energy.KE) + " KP übrig.";
	  return costs;
  }

  /**
   * This method initializes jButton1
   * 
   * @return javax.swing.JButton
   */
  private JButton getCancelButton() {
    if (cancelButton == null) {
      cancelButton = new JButton();
      cancelButton.setPreferredSize(new java.awt.Dimension(90, 25));
      cancelButton.setText("Abbruch");
      cancelButton.setMnemonic(java.awt.event.KeyEvent.VK_A);
      cancelButton.setLocation(164, 270);
      cancelButton.setSize(90, 25);
      cancelButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          result = Result.Canceled;
          LiturgyProbeDialog.this.dispose();
        }
      });
    }
    return cancelButton;
  }

  /**
   * This method initializes jPanel
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getJPanel() {
    if (jPanel == null) {
      propertyLabel = new JLabel();
      propertyLabel.setBounds(new java.awt.Rectangle(12, 44, 222, 20));
      propertyLabel.setText("Probe geht auf:");
      increaseBox = new JCheckBox();
      increaseBox.setBounds(new java.awt.Rectangle(12, 95, 226, 20));
      increaseBox.setText("Ziel aufstufen");
      increaseBox.addActionListener(new java.awt.event.ActionListener() {
		@Override
		public void actionPerformed(java.awt.event.ActionEvent arg0) {
			costLabel.setText("Kosten: " + getCosts(getRealDegree()));
		    defaultAddLabel.setText("Normaler Zuschlag für Probe: "
		            + getModifier(getRealDegree()));
	        int mod = getModifier(getRealDegree());
	        if (liturgy.getTestData().hasHalfStepLess()) {
	          mod -= hero.getStep() / 2;
	        }
	        getDifficultySpinner().setValue(Integer.valueOf(mod));
			setProbability();
			boolean probeAllowed = getKECost(getRealDegree()) <= hero.getCurrentEnergy(Energy.KE);
			getProbeButton().setEnabled(probeAllowed);
		}
      });
      halfStepLabel = new JLabel();
      halfStepLabel.setBounds(new java.awt.Rectangle(12, 125, 226, 20));
      halfStepLabel.setText("Reduzierung um halbe Stufe: ");
      modifiersLabel = new JLabel();
      modifiersLabel.setBounds(new java.awt.Rectangle(12, 155, 270, 20));
      modifiersLabel.setText("Bitte Modifikatoren nach KKO S. 11 beachten!");
      defaultAddLabel = new JLabel();
      defaultAddLabel.setBounds(new java.awt.Rectangle(12, 185, 224, 20));
      defaultAddLabel.setText("Normaler Zuschlag für Probe: ");
      costLabel = new JLabel();
      costLabel.setBounds(new java.awt.Rectangle(12, 215, 224, 20));
      costLabel.setText("Kosten: " + getCosts(liturgy.getDegree()));
      jLabel2 = new JLabel();
      jPanel = new JPanel();
      jLabel = new JLabel();
      jPanel.setLayout(null);
      jPanel.setBounds(17, 14, 290, 250);
      jPanel.setBorder(javax.swing.BorderFactory
          .createEtchedBorder(javax.swing.border.EtchedBorder.LOWERED));
      jLabel.setBounds(12, 12, 62, 19);
      jLabel.setText("Zuschlag:");
      // jLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
      jPanel.add(getDifficultySpinner(), null);
      jPanel.add(jLabel, null);
      jLabel.setLabelFor(getDifficultySpinner());
      jLabel2.setBounds(12, 69, 224, 20);
      jLabel2.setText("Erfolgswahrscheinlichkeit");
      jLabel2.setForeground(java.awt.Color.RED);
      jPanel.add(jLabel2, null);
      jPanel.add(defaultAddLabel, null);
      jPanel.add(halfStepLabel, null);
      jPanel.add(propertyLabel, null);
      jPanel.add(increaseBox, null);
      jPanel.add(modifiersLabel, null);
      jPanel.add(costLabel, null);
    }
    return jPanel;
  }
  
  private static String getCosts(int degree) {
	  if (degree < 5)
		  return "" + degree * 6 + " KP";
	  else if (degree == 5)
		  return "36 KP, davon 1 permanent";
	  else if (degree == 6)
		  return "48 KP, davon 3 permanent";
	  else
		  return "60 KP, davon 6 permanent";
  }
  
  private static int getKECost(int degree) {
	  if (degree < 5)
		  return degree * 6;
	  else if (degree == 5)
		  return 36;
	  else if (degree == 6)
		  return 48;
	  else
		  return 60;
  }
  
  private static int getPermanentKECost(int degree) {
	  if (degree < 5)
		  return 0;
	  else if (degree == 5)
		  return 1;
	  else if (degree == 6)
		  return 3;
	  else
		  return 6;
  }
  
  private static int getModifier(int degree) {
	  if (degree < 7)
		  return (degree - 1) * 3;
	  else
		  return 21;
  }
  
  private int getRealDegree() {
	  if (increaseBox.isSelected())
		  return liturgy.getDegree() + 1;
	  else
		  return liturgy.getDegree();
  }

  /**
   * This method initializes jSpinner
   * 
   * @return javax.swing.JSpinner
   */
  private JSpinner getDifficultySpinner() {
    if (difficultySpinner == null) {
      difficultySpinner = new JSpinner();
      difficultySpinner.setSize(43, 20);
      difficultySpinner.setLocation(81, 12);
      difficultySpinner.setModel(new SpinnerNumberModel(0, -50, 80, 1));
      difficultySpinner.addChangeListener(new ChangeListener() {
        public void stateChanged(ChangeEvent e) {
          setProbability();
        }
      });
    }
    return difficultySpinner;
  }

  public String getHelpPage() {
    return "Probe";
  }
} //  @jve:decl-index=0:visual-constraint="10,10"
